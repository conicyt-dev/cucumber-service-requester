var q = require('q');
var request = require('request');
var _ = require('lodash');
const assert = require('assert');

module.exports = {
    Requester: function Requester(apiBaseURL, credentials) {
        var accessToken = null;
        var lastResponse = null;

        this.response = new Respuesta();

        this.token = function () {
            var defer = q.defer();

            request({
                    method: 'POST',
                    url: apiBaseURL + '/token',
                    headers: {
                        'Authorization': 'Basic ' + credentials
                    },
                    form: {
                        'grant_type': 'client_credentials'
                    }
                },
                function (err, response, body) {
                    if (err) {
                        return defer.reject(err);
                    }

                    if (response.statusCode != 200) {
                        return defer.reject(body)
                    }

                    defer.resolve(accessToken = JSON.parse(body).access_token);
                });

            return defer.promise;
        };

        this.request = function (method, url) {
            var defer = q.defer();

            if (_.isEmpty(accessToken)) {
                defer.reject('no hay token de acceso');
            }
            else {
                request({
                        method: method,
                        url: apiBaseURL + url,
                        headers: {
                            'Authorization': 'Bearer ' + accessToken
                        }
                    },
                    function (err, response, body) {
                        if (err) {
                            return defer.reject(err);
                        }

                        if (response.statusCode != 200) {
                            return defer.reject(body);
                        }

                        lastResponse = response;

                        defer.resolve(lastResponse);
                    });
            }

            return defer.promise;
        };

        function Respuesta() {

            this.body = new Body();

            function Body() {

                /**
                 * Chequea si el objeto dado esta contenido en el primer nivel
                 * @param objeto
                 */
                this.hasObjectWithProperties = function (objeto) {
                    var parsed = JSON.parse(lastResponse.body);

                    var actual = parsed;
                    var expected = JSON.parse(objeto);

                    _.each(expected,
                        function (value, field) {
                            var obj = {};
                            obj[field] = value;
                            actual = _.filter(actual, obj);
                            assert.ok(!_.isEmpty(actual), JSON.stringify({actual: parsed, expected: expected}));
                        });
                };

                /**
                 * Chequea si el objeto dado esta contenido en la variable en el primer nivel
                 * @param variable
                 * @param objeto
                 */
                this.hasObjectWithPropertiesInVariable = function (variable, objeto) {
                    var response = JSON.parse(lastResponse.body);

                    var actual = null;
                    eval("actual = a." + variable + ";");

                    _.each(JSON.parse(objeto),
                        function (value, field) {
                            var obj = {};
                            obj[field] = value;
                            actual = _.filter(actual, obj);
                            assert.ok(!_.isEmpty(actual), JSON.stringify({actual: response, expected: objeto}));
                        });
                };

                /**
                 * Chequea si el cuerpo es nativo o es un objeto plano
                 */
                this.isPlainObject = function () {
                    var actual = JSON.parse(lastResponse.body);
                    assert.ok(_.isNative(actual) || (_.isObject(actual) && !_.isArray(actual)));
                };

                /**
                 * Chequea si el cuerpo es igual al objeto dado
                 * @param objeto
                 */
                this.isObjectWithProperties = function (objeto) {
                    assert.deepEqual(
                        sort(JSON.parse(lastResponse.body)),
                        sort(JSON.parse(objeto)))
                };

                /**
                 * Chequea si el cuerpo es un arreglo con el tamaño dado
                 * @param size
                 */
                this.isArrayWithSize = function (size) {
                    assert.equal(_.size(JSON.parse(lastResponse.body)), size);
                };

                /**
                 * Chequea si la variable dada es un arreglo con el tamaño dado
                 * @param variable
                 * @param size
                 */
                this.isArrayWithSizeInVariable = function (variable, size) {
                    var a = JSON.parse(lastResponse.body);
                    var actual = null;
                    eval("actual = a." + variable + ";");

                    assert.equal(_.size(actual), size);
                };
            }

            /**
             * Chequea el status de la respuesta
             * @param statusCode
             */
            this.hasStatusCode = function (statusCode) {
                return assert.equal(lastResponse.statusCode, statusCode);
            };
        }
    }

};


function sort(obj) {
    if (_.isArray(obj)) {
        return sortArray(obj);
    }

    if (_.isObject(obj)) {
        return sortObject(obj);
    }

    return obj;
}

function sortArray(actual) {
    return _.map(
        _.sortBy(
            _.map(
                actual,
                function (obj) {
                    return JSON.stringify(sortObject(obj));
                })),
        function (obj) {
            return JSON.parse(obj);
        });
}

function sortObject(actual) {
    if (_.isObject(actual)) {
        var sorted = _(actual).toPairs().sortBy(0).fromPairs().value();

        var result = {};
        _.each(sorted, function (value, key) {
            result[key] = sort(value);
        });
        return result;
    }
    else {
        return actual;
    }
}